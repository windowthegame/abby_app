class Api::TodoList::UsersController < Api::BaseController

  before_filter :authentication_user_with_authentication_token

  def update_profile
    if params[:avatar].present?
      @current_user.convert(params[:avatar])
    end
    if @current_user.update_attributes(user_params) && @current_user.save
      render :file => "api/todo_list/users/profile"
    else
      render_json({:errors => @current_user.display_errors, :status => 404}.to_json)
    end
  end

  def get_profile
    @user = @current_user
    render :file => "api/todo_list/users/profile"
  end

  def search
    if params[:search_string].present? && params[:search_type]
      @users = User.search(params[:search_string],params[:search_type],@current_user.id)
      if @users.present?
        render :file => "api/todo_list/friends/index"
      else
        render_json({:errors => "No users found", :status => 404}.to_json)
      end
    else
      render_json({:errors => "Parameters search_number and search_string are required.", :status => 404}.to_json)
    end
    # if params[:search_string].present?
    #   @users = User.search params[:search_string], where: { id: {not: current_user.id} }
    #   if @users.present?
    #     render :file => "api/todo_list/friends/index"
    #   else
    #     render_json({:errors => "No users found", :status => 404}.to_json)
    #   end
    # else
    #   render_json({:errors => "Parameter search_string required.", :status => 404}.to_json)
    # end
  end

  def user_params
    params.require(:user).permit(:username, :firstname, :lastname, :device_type, :device_id,:contact_number)
  end

end