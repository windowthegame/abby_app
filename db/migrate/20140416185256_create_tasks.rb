class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.integer 	:user_id
      t.string 		:task_name
      t.string 		:status
      t.integer 	:point, default: 0
      t.timestamps
    end
  end
end
