class AddColumnInnerTaskToTask < ActiveRecord::Migration
  def change
    add_column :tasks, :inner_task, :string, :default => ""
  end
end
